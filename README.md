# Solutions  
_Collection of solutions from learning code_  

---  

## Usage:  
1. clone this repo:  
```bash
git clone https://codeberg.org/deftclaw/solutions
```  
2. clone the tutorial repo:  
```bash
git clone https://codeberg.org/ziglings/exercises
```  
3. use `pushd` instead of `cd` because we will return alot:  
```bash
pushd solutions
pushd ../exercises
```  
4. confirm your dependencies:  
```bash
$ zig version
0.12.0-dev.3365+cbeab678a
```  
5. test a solution patch:  
```bash
git apply $(dirs +1)/ziglings/001_hello.patch
```  
6. Confirm it worked:  
```bash
$ zig build
...
PASSED:
Hello world!
```   
